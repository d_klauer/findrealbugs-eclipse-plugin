/* License (BSD Style License):
 * Copyright (c) 2009 - 2014
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the Software Technology Group or Technische
 *    Universität Darmstadt nor the names of its contributors may be used to
 *    endorse or promote products derived from this software without specific
 *    prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package de.tud.cs.st.bat
package findrealbugs
package plugin

import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.jdt.core._
import org.eclipse.jdt.core.dom._

/**
 * Provides utility functions to locate source files and line numbers.
 *
 * @author Florian Brandherm
 */
object ResourceLocationUtility {

    /**
     * Creates an abstract source tree from an `IMember`.
     * It parses the corresponding compilation unit and generates bindings.
     *
     * @param elem an `IMember` object, whose compilation unit should be parsed.
     * This is typically used with an `IType`, `IField` or `IMethod`.
     * @return `Some(CompilationUnit)` if the source file could be parsed. `None`, if not
     * (if it's not a java file).
     */
    private def createAbstractSourceTree(elem: IMember): Option[CompilationUnit] = {
        if (elem.getCompilationUnit().getPath().getFileExtension() == "java") {
            val parser: ASTParser = ASTParser.newParser(AST.JLS4)
            parser.setKind(ASTParser.K_COMPILATION_UNIT)
            parser.setSource(elem.getCompilationUnit())
            //generate bindings
            parser.setResolveBindings(true)
            parser.setBindingsRecovery(true)
            Some(parser.createAST(null).asInstanceOf[CompilationUnit])
        } else {
            None
        }
    }

    /**
     * Tries to find the type of a class with a given name in a given project.
     * Recursively searches all required projects.
     *
     * @param project The project that is associated with the search query.
     * @param className Name of the class that should be found.
     * @param alreadySearchedProjectNames Mutable `Set` of the names of all projects
     * that have already been searched. This is to prevent non-termination due to
     * cyclic project dependencies.
     * @return The `IType` of the searched class, `null` if it wasn't found.
     */
    def findType(
        project: IJavaProject,
        className: String,
        alreadySearchedProjects: scala.collection.mutable.Set[IJavaProject] = scala.collection.mutable.Set()): IType = {

        var classType = project.findType(className)
        alreadySearchedProjects.add(project)

        // If the classType wasn't found in this project, look it up in 
        // required projects
        if (classType == null) {
            val projects = ResourcesPlugin.getWorkspace.getRoot.getProjects
            val requiredProjectNames = project.getRequiredProjectNames()

            // Iterate through all required and found java projects
            for {
                requiredProjectName ← requiredProjectNames
                if classType == null
                projectOption = projects.find(_.getName() == requiredProjectName)
                project <- projectOption
                if project.isNatureEnabled("org.eclipse.jdt.core.javanature")
            } {
                val javaProject = JavaCore.create(project)
                //don't search projects that were already searched before to 
                //prevent non-termination and stack overflow
                if (!alreadySearchedProjects.contains(javaProject)) {
                    classType =
                        findType(javaProject, className,
                            alreadySearchedProjects)

                }
            }
        }

        classType
    }

    /**
     * Determines the line number of a class declaration.
     *
     * @param className Name of the class for which the line number of the declaration
     * should be determined.
     * @param project The java project that the given class belongs to
     * @return `Some(line number)` if it can be determined. `None` otherwise.
     */
    def getClassLineNumber(className: String, project: IJavaProject): Option[Int] = {
        val classType = findType(project, className)
        if (classType != null) {
            getLineNumber(classType)
        } else {
            None
        }
    }

    /**
     * Determines the line number of a method declaration.
     *
     * @param className Name of the class that contains the method.
     * @param methodName Name of the method for which the line number should be
     * determined.
     * @param parameterSignature Array of the methods parameter types in JVM syntax.
     * @param project The java project that the given class belongs to.
     * @return `Some(line number)` if it can be determined. `None` otherwise.
     */
    def getMethodLineNumber(
        className: String,
        methodName: String,
        parameterSignature: Array[String],
        project: IJavaProject): Option[Int] = {
        val classType = findType(project, className)
        if (classType != null) {
            getLineNumber(classType.getMethod(methodName, parameterSignature))
        } else {
            None
        }
    }

    /**
     * Determines the line number of a field declaration.
     *
     * @param className Name of the class that contains the method.
     * @param fieldName Name of the field for which the line number should be determined.
     * @param project The java project that the given class belongs to.
     * @return `Some(line number)` if it can be determined. `None` otherwise.
     */
    def getFieldLineNumber(
        className: String,
        fieldName: String,
        project: IJavaProject): Option[Int] = {
        val classType = findType(project, className)
        if (classType != null) {
            getLineNumber(classType.getField(fieldName))
        } else {
            None
        }
    }

    /**
     * Returns the line number of a class declaration.
     *
     * @param classType `IType` for the class for which the line number should be
     * determined.
     * @return `Some(line number)` if it can be determined. `None` otherwise.
     */
    private def getLineNumber(classType: IType): Option[Int] = {
        if (classType == null) {
            return None
        }

        val className = classType.getFullyQualifiedName()
        val compilationUnitOption = createAbstractSourceTree(classType)
        if (!compilationUnitOption.isDefined) {
            return None
        }

        var linenumberOption: Option[Int] = None

        compilationUnitOption.get.accept(new ASTVisitor {
            /**
             * Gets called once for each type declaration in the java file.
             *
             * @param node Abstract source tree node that corresponds to the type
             * declaration.
             */
            override def visit(node: dom.TypeDeclaration): Boolean = {
                if (node != null) {
                    val name = node.resolveBinding().getJavaElement().
                        asInstanceOf[IType].getFullyQualifiedName()
                    if (name == className) {
                        linenumberOption = Some(compilationUnitOption.get.
                            getLineNumber(node.getStartPosition()))
                    }
                    super.visit(node)
                } else {
                    false
                }
            }
        })

        linenumberOption
    }

    /**
     * Returns the line number of a method declaration.
     *
     * @param classType `IMethod` for the method for which the line number should be
     * determined.
     * @return `Some(line number)` if it can be determined. `None` otherwise.
     */
    private def getLineNumber(method: IMethod): Option[Int] = {
        if (method == null) {
            return None
        }

        val compilationUnitOption = createAbstractSourceTree(method)
        if (!compilationUnitOption.isDefined) {
            return None
        }

        var linenumberOption: Option[Int] = None

        compilationUnitOption.get.accept(new ASTVisitor {
            /**
             * Gets called once for each method declaration in the java file.
             *
             * @param node Abstract source tree node that corresponds to the method
             * declaration.
             */
            override def visit(node: MethodDeclaration): Boolean = {
                if (node != null) {
                    if (method.isSimilar(node.resolveBinding().getJavaElement().
                        asInstanceOf[IMethod])) {
                        linenumberOption =
                            Some(compilationUnitOption.get.
                                getLineNumber(node.getStartPosition()))
                    }
                    super.visit(node)
                } else {
                    false
                }
            }
        })

        linenumberOption
    }

    /**
     * Returns the line number of a field declaration.
     *
     * @param classType `IField` for the field for which the line number should be
     * determined.
     * @return `Some(line number)` if it can be determined. `None` otherwise.
     */
    private def getLineNumber(field: IField): Option[Int] = {
        if (field == null) {
            return None
        }

        val compilationUnitOption = createAbstractSourceTree(field)
        if (!compilationUnitOption.isDefined) {
            return None
        }

        val fieldName = field.getElementName()
        var linenumberOption: Option[Int] = None

        compilationUnitOption.get.accept(new ASTVisitor {
            /**
             * Gets called once for each field declaration in the java file.
             *
             * @param node abstract source tree node that corresponds to the field
             * declaration.
             */
            override def visit(node: FieldDeclaration): Boolean = {
                if (node != null) {
                    val frag = node.fragments().get(0)
                    val name = frag.asInstanceOf[VariableDeclarationFragment].getName().
                        toString()
                    if (name == fieldName) {
                        linenumberOption =
                            Some(compilationUnitOption.get.getLineNumber(
                                node.getStartPosition()))
                    }
                    super.visit(node)
                } else {
                    false
                }
            }
        })

        linenumberOption
    }
}
