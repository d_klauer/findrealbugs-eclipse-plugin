/* License (BSD Style License):
 * Copyright (c) 2009 - 2014
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the Software Technology Group or Technische
 *    Universität Darmstadt nor the names of its contributors may be used to
 *    endorse or promote products derived from this software without specific
 *    prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package de.tud.cs.st
package bat
package findrealbugs
package plugin
package test

import org.scalatest._
import org.eclipse.core.resources.IProject
import org.eclipse.jdt.core.IJavaProject
import java.lang.reflect.{ Proxy, InvocationHandler, Method }

/**
 * This trait provides helper functions for unit tests concerning the eclipse plugin.
 *
 * @author Florian Brandherm
 */
trait PluginTest extends FlatSpec with Matchers with ParallelTestExecution {

    /**
     * This creates a mock `IJavaProject` object which only implements methods getProject
     * and equals. A mock of interface IProject which only implements method getName will
     * be returned by IJavaProject.getProject.
     *
     * It assigns an identifying name to the IProject which is used for comparison.
     *
     * @param name name of the mock project
     * @return mock `IJavaProject`
     */
    def createMockJavaProject(name: String): IJavaProject = {
        // Create proxy for the IProject inside the IJavaProject
        val iproject = Proxy.newProxyInstance(getClass().getClassLoader(),
            Array[Class[_]](classOf[IProject]), // Implemented interface
            new InvocationHandler() {
                override def invoke(
                    proxy: Object,
                    method: Method,
                    args: Array[Object]): Object = {
                    method.getName match {
                        case "getName" ⇒ name
                        case _         ⇒ null
                    }
                }
            }).asInstanceOf[IProject]

        // Create proxy for the IJavaProject
        val ijavaProject = Proxy.newProxyInstance(getClass().getClassLoader(),
            Array[Class[_]](classOf[IJavaProject]),
            new InvocationHandler() {
                override def invoke(
                    proxy: Object,
                    method: Method,
                    args: Array[Object]): Object = {
                    method.getName match {
                        case "getProject" ⇒ iproject
                        case "equals" ⇒ (args(0).isInstanceOf[IJavaProject] &&
                            args(0).asInstanceOf[IJavaProject].getProject().
                            getName() == name).asInstanceOf[Object]
                        case _ ⇒ null
                    }
                }
            }).asInstanceOf[IJavaProject]

        // Assert that the mock object was successfully created
        ijavaProject should not be null

        ijavaProject
    }
}
